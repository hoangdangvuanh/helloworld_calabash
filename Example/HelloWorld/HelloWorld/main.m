//
//  main.m
//  HelloWorld
//
//  Created by Hoang Dang Vu Anh on 6/22/15.
//  Copyright (c) 2015 Hoang Dang Vu Anh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
