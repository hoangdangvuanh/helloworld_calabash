//
//  HelloWorldVC.m
//  GDOScore
//
//  Created by Hoang Dang Vu Anh on 6/22/15.
//  Copyright (c) 2015 Nguyen Khac Trung. All rights reserved.
//

#import "HelloWorldVC.h"

@interface HelloWorldVC ()
<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *tfHelloWorld;
    
}
@end

@implementation HelloWorldVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
