require 'calabash-cucumber/management/adb'
require 'calabash-cucumber/operations'

Before do |scenario|
  start_test_server_in_background
end

After do |scenario|
  if scenario.failed?
    screenshot_embed
  end
  shutdown_test_server
end
