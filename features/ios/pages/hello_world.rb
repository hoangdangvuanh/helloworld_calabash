require 'calabash-cucumber/ibase'

class HelloWorld < Calabash::ibase

  def trait
  
	check_text
  end

  def check_text
    hide_soft_keyboard
    function_get_text
  end

  def function_get_text
    result = nil
    wait_for(timeout: 120) do
      if element_exists("* marked:'Hello World'")
        result = :valid
      end
    end

    case result
      when :invalid
        self
      else
         @csv = page(ReadFileCSV).set_path("data/hello_world.csv")#"hello_world.csv"
		 arr_data_CSV = @csv.get_data_from_CSV()
         if "Hello World" == arr_data_CSV[0]
         	shutdown_test_server
         end
       end
	end
end