﻿# coding: utf-8
require 'calabash-cucumber/ibase'
require 'logger'
require 'csv'

class ReadFileCSV < Calabash::ibase
  def initialize(logfile = 'log/read_file_csv.log')
    #ham khoi tao cua ruby
    file = File.open('log/read_file_csv.log', File::APPEND | File::CREAT)
    @logger = Logger.new('log/read_file_csv.log')
    @filePath = ""
    @errorMessage = ""
    @errorCode = 0 #khong co loi
  end

  def logger
    #lay bien the hien
    @logger
  end

  def get_error()
    # Lay message loi.
    @errorMessage
  end

  def set_path(path_to_file)
    @errorCode = 0
    @filePath = path_to_file
    puts @filePath
    self
  end

  def read_file_csv(path_to_file)
    CSV.foreach(path_to_file) do |row|
      # use row here...
      logger::debug("read_file_csv  row #{row}")
      #  return row
    end
  end

  def get_data_from_CSV()
    index = 0
    arr_data_CSV = []
    CSV.foreach(@filePath,"r:UTF-8") do |row|
      arr_data_CSV[index] = row
      index = index + 1
    end
    return arr_data_CSV
  end

  def get_and_check_item_data(row)
    obj_scoreinput = ""
    if (row.nil?) then
      obj_scoreinput = ""
    else
      obj_scoreinput = row
    end
    return obj_scoreinput
  end

  def get_cell_by_name_index(column_name, search_row_index)
    # search_row_index bat dau tu 1 .. n. Muon tim dong 1 thi truyen 1 vao.
    # column_name ten cua column muon tim kiem.
    @errorCode = 0
    logger::debug("get_cell_by_name_index  column_name #{column_name} search_row_index #{search_row_index}")
    # truyen vao column nao, index nao se lay ra duoc text nhu vay.
    # index start of array is 0.
    # mac dinh dong dau tien la dong title se khong xet dong nay.
    if @filePath.empty? then
      @errorCode = -1
      @errorMessage = "Path to file not set."
      return
    end
    search_column = column_name.encode("utf-8")
    begin
      rowIndex = 0
      foundString = ""
      column_index = -1
      logger::debug("get_cell_by_name_index #1")
      CSV.foreach(@filePath,"r:UTF-8") do |row|
        logger::debug("get_cell_by_name_index rowIndex #{rowIndex}")
        # use row here...
        if rowIndex == 0 then
          #title
          column_index = get_index(row, search_column)
        end
        if rowIndex == search_row_index && rowIndex > 0 then
          foundString = row.at(column_index)
          logger::debug("get_cell_by_name_index found #{search_column} column_index #{column_index} of row #{search_row_index} value #{foundString.encode("utf-8")}")
          logger::debug(foundString)
          return foundString
        end
        logger::debug("get_cell_by_name_index  row #{row}")
        rowIndex = rowIndex + 1
      end
    rescue Exception => e
      logger::debug("get_cell_by_name_index  error #{e.message}")
      @errorMessage = e.message
      @errorCode = -1
    end
    return foundString
  end

  def get_index(array_value, value)
    # lay index cua [value] trong array
    count = array_value.count
    if count < 1 then
      return -1
    end
    index = 0
    array_value.each do |val|
      #logger::debug("with #{val}")
      if val.encode("utf-8").index(value) != nil then #update 08/07/2014
        logger::debug("get_index **found**")
        return index
      end
      index = index + 1
    end
    logger::debug("get_index  value #{value} NOT found")
    return -1
  end
end